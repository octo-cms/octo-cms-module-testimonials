<?php

namespace OctoCmsModule\Testimonials\Tests\Commands;

use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class FakeTestimonialsCommandTes
 *
 * @package OctoCmsModule\Services\Tests\Commands
 */
class InstallTestimonialsCommandTest extends TestCase
{


    public function test_fakeTestimonialCommand() {

        $this->artisan('install:testimonials')
            ->expectsOutput('Running Install Testimonials Command ...')
            ->expectsOutput('Creating Settings ...')
            ->assertExitCode(0);

        $this->assertDatabaseHas('settings', [
            'name' => 'module_testimonials'
        ]);
    }
}
