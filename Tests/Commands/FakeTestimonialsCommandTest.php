<?php

namespace OctoCmsModule\Testimonials\Tests\Commands;

use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class FakeTestimonialsCommandTes
 *
 * @package OctoCmsModule\Services\Tests\Commands
 */
class FakeTestimonialsCommandTest extends TestCase
{


    public function test_fakeTestimonialCommand() {

        $this->artisan('fake:testimonials')
            ->expectsOutput('Running Fake Testimonials Command ...')
            ->expectsOutput('Creating Testimonials ...')
            ->assertExitCode(0);


        $this->assertCount(
            20,
            Testimonial::all()
        );

    }
}
