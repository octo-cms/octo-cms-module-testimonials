<?php

namespace OctoCmsModule\Testimonials\Tests\Services\TestimonialService;

use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;
use OctoCmsModule\Testimonials\Services\TestimonialService;

/**
 * Class SaveTestimonialTest
 *
 * @package OctoCmsModule\Testimonials\Tests\Services\TestimonialService
 */
class SaveTestimonialTest extends TestCase
{



    public function test_storeTestimonial()
    {
        $fields = [
            'author'           => 'Pinco Pallino',
            'testimonialLangs' => [
                [
                    'lang' => 'it',
                    'job'  => 'professore',
                    'text' => 'Sono un professore',
                ],
                [
                    'lang' => 'en',
                    'job'  => 'teacher',
                    'text' => 'I am a teacher',
                ],
            ],
        ];

        /** @var TestimonialService $testimonialService */
        $testimonialService = new TestimonialService();

        $testimonial = new Testimonial();

        $testimonialService->saveTestimonial($testimonial, $fields);

        $this->assertDatabaseHas('testimonials', [
            'id'     => 1,
            'author' => $fields['author'],
        ]);

        foreach ($fields['testimonialLangs'] as $testimonialLang) {
            $this->assertDatabaseHas('testimonial_langs',
                [
                    'testimonial_id' => 1,
                    'lang'           => $testimonialLang['lang'],
                    'job'            => $testimonialLang['job'],
                    'text'           => $testimonialLang['text'],
                ]
            );
        }
    }

    public function test_updateTestimonial()
    {
        /** @var array $fields */
        $fields = [
            'author'           => 'Pinco Pallino',
            'testimonialLangs' => [
                [
                    'lang' => 'it',
                    'job'  => 'professore',
                    'text' => 'Sono un professore',
                ],
                [
                    'lang' => 'en',
                    'job'  => 'teacher',
                    'text' => 'I am a teacher',
                ],
            ],
        ];

        /** @var Testimonial $testimonial */
        $testimonial = Testimonial::factory()
            ->has(
                TestimonialLang::factory()->state([
                    'lang' => 'it',
                    'job'  => 'ingegnere',
                    'text' => 'Sono un ingegnere',
                ])
            )->has(
                TestimonialLang::factory()->state([
                    'lang' => 'en',
                    'job'  => 'engineer',
                    'text' => 'I am an engineer',
                ])
            )->create(['author' => 'Mario Rossi']);


        /** @var TestimonialService $testimonialService */
        $testimonialService = new TestimonialService();

        $testimonialService->saveTestimonial($testimonial, $fields);

        $this->assertDatabaseHas('testimonials', [
            'id'     => $testimonial->id,
            'author' => $fields['author'],
        ]);

        foreach ($fields['testimonialLangs'] as $testimonialLang) {
            $this->assertDatabaseHas('testimonial_langs',
                [
                    'testimonial_id' => 1,
                    'lang'           => $testimonialLang['lang'],
                    'job'            => $testimonialLang['job'],
                    'text'           => $testimonialLang['text'],
                ]
            );
        }
    }
}
