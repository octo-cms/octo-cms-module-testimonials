<?php

namespace OctoCmsModule\Testimonials\Tests\Mocks\Services;

use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Interfaces\TestimonialServiceInterface;

class TestimonialServiceMock implements TestimonialServiceInterface
{
    public function saveTestimonial(Testimonial $testimonial, array $fields): Testimonial
    {
        return factory(Testimonial::class,1)->create()->first();
    }
}
