<?php

namespace OctoCmsModule\Testimonials\Tests\Mocks\Services;

use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Tests\Mocks\PictureServiceMock as PictureServiceMockCore;
use OctoCmsModule\Testimonials\Interfaces\PictureServiceInterface;

/**
 * Class PictureServiceMock
 *
 * @package OctoCmsModule\Testimonials\Tests\Mocks\Services
 */
class PictureServiceMock extends PictureServiceMockCore implements PictureServiceInterface
{


}
