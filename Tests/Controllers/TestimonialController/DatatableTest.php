<?php

    namespace OctoCmsModule\Testimonials\Tests\Controllers\TestimonialController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Testimonials\Entities\Testimonial;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Testimonials\Tests\Controllers\TestimonialController
 */
class DatatableTest extends TestCase
{


    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        Testimonial::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.testimonials');
    }
}
