<?php

namespace OctoCmsModule\Testimonials\Tests\Controllers\TestimonialController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Testimonials\Entities\Testimonial;


/**
 * Class UpdateTest
 *
 * @package OctoCmsModule\Testimonials\Tests\Controllers\TestimonialController
 */
class UpdateTest extends TestCase
{



    public function dataProvider()
    {
        $providers = [];

        $data = [
            'author'           => 'Pinco Pallino',
            'testimonialLangs' => [
                [
                    'lang' => 'it',
                    'job'  => 'professore',
                    'text' => 'Sono un professore',
                ],
                [
                    'lang' => 'en',
                    'job'  => 'teacher',
                    'text' => 'I am a teacher',
                ],
            ],
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields['author']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['testimonialLangs']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['testimonialLangs'][0]['lang']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['testimonialLangs'][1]['job']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['testimonialLangs'][0]['text']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_update(array $fields, int $status)
    {
        /** @var Testimonial $testimonial */
        $testimonial = Testimonial::factory()->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'PUT',
            route('admin.testimonials.update', ['id' =>  $testimonial->id]),
            $fields
        );

        $response->assertStatus($status);
    }
}
