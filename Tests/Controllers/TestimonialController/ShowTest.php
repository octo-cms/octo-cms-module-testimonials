<?php

namespace OctoCmsModule\Testimonials\Tests\Controllers\TestimonialController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;

/**
 * Class ShowTest
 *
 * @package OctoCmsModule\Testimonials\Tests\Controllers\TestimonialController
 */
class ShowTest extends TestCase
{


    public function test_show()
    {
        /** @var Testimonial $testimonial */
        $testimonial = Testimonial::factory()->has(TestimonialLang::factory()->count(2))->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('admin.testimonials.show', ['id' =>  $testimonial->id])
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
            'id'     => $testimonial->id,
            'author' => $testimonial->author,
        ]);
    }
}
