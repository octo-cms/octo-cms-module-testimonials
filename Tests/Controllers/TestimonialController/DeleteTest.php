<?php

namespace OctoCmsModule\Testimonials\Tests\Controllers\TestimonialController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;

/**
 * Class DeleteTest
 *
 * @package OctoCmsModule\Testimonials\Tests\Controllers\TestimonialController
 */
class DeleteTest extends TestCase
{


    public function test_delete()
    {
        /** @var Testimonial $testimonial */
        $testimonial = Testimonial::factory()->has(TestimonialLang::factory()->count(2))->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'DELETE',
            route('admin.testimonials.delete', ['id' => $testimonial->id])
        );

        $response->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('testimonials', [
            'id'     => $testimonial->id,
            'author' => $testimonial->author,
        ]);

        $this->assertDatabaseMissing('testimonial_langs', [
            'testimonial_id' => $testimonial->id,
        ]);
    }
}
