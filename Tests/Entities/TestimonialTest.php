<?php

namespace OctoCmsModule\Testimonials\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class TestimonialTest
 *
 * @package OctoCmsModule\Testimonials\Tests\Entities
 */
class TestimonialTest extends TestCase
{


    public function test_TestimonialHasManyTestimonialLangs()
    {
        /** @var Testimonial $testimonial */
        $testimonial = Testimonial::factory()->has(TestimonialLang::factory()->count(2))->create()->first();

        $testimonial->load('testimonialLangs');

        $this->assertInstanceOf(Collection::class, $testimonial->testimonialLangs);
        $this->assertInstanceOf(TestimonialLang::class, $testimonial->testimonialLangs->first());
    }
}
