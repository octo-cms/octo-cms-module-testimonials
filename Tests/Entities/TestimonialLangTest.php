<?php

namespace OctoCmsModule\Testimonials\Tests\Entities;

use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class TestimonialLangTest
 *
 * @package OctoCmsModule\Testimonials\Tests\Entities
 */
class TestimonialLangTest extends TestCase
{


    public function test_TestimonialLangBelongsToTestimonial()
    {
        /** @var TestimonialLang $testimonialLang */
        $testimonialLang = TestimonialLang::factory()->create();

        $testimonialLang->load('testimonial');

        $this->assertInstanceOf(Testimonial::class, $testimonialLang->testimonial);
    }
}
