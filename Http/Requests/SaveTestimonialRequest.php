<?php

namespace OctoCmsModule\Testimonials\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Traits\SavePictureRequestTrait;

/**
 * Class SaveTestimonialRequest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Testimonials\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srlù
 */
class SaveTestimonialRequest extends FormRequest
{
    use SavePictureRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Arr::collapse([
            [
                'author'                  => 'required|string',
                'testimonialLangs'        => 'required|array',
                'testimonialLangs.*.lang' => 'required|string',
                'testimonialLangs.*.job'  => 'required|string',
                'testimonialLangs.*.text' => 'required|string',
            ], $this->getPictureRules()
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'author.required'                  => __('testimonials::validation.author.required'),
            'author.string'                    => __('testimonials::validation.author.string'),
            'testimonialLangs.required'        => __('testimonials::validation.testimonialLangs.required'),
            'testimonialLangs.*.lang.required' => __('testimonials::validation.testimonialLangs.*.lang.required'),
            'testimonialLangs.*.lang.string'   => __('testimonials::validation.testimonialLangs.*.lang.string'),
            'testimonialLangs.*.job.required'  => __('testimonials::validation.testimonialLangs.*.job.required'),
            'testimonialLangs.*.job.string'    => __('testimonials::validation.testimonialLangs.*.job.string'),
            'testimonialLangs.*.text.required' => __('testimonials::validation.testimonialLangs.*.text.required'),
            'testimonialLangs.*.text.string'   => __('testimonials::validation.testimonialLangs.*.text.string'),
        ];
    }
}
