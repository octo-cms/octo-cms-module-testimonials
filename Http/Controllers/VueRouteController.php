<?php

namespace OctoCmsModule\Testimonials\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 * Class VueRouteController
 *
 * @category Octo
 * @package  OctoCmsModule\Testimonials\Http\Controllers
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class VueRouteController extends Controller
{
    /**
     * Name testimonials
     *
     * @return mixed
     */
    public function testimonials()
    {

        return view(
            'admin::vue-full-page',
            ['script' => 'testimonials/testimonials']
        );
    }

    /**
     * @return mixed
     */
    public function settings()
    {

        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'testimonials/settings']);
    }
}
