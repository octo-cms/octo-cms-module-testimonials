<?php

namespace OctoCmsModule\Testimonials\Http\Controllers\V1;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Testimonials\Http\Requests\SaveTestimonialRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Testimonials\Interfaces\PictureServiceInterface;
use OctoCmsModule\Testimonials\Interfaces\TestimonialServiceInterface;
use OctoCmsModule\Testimonials\Transformers\TestimonialResource;

/**
 * Class TestimonialController
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Testimonials\Http\Controllers\V1
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class TestimonialController extends Controller
{
    /**
     * TestimonialServiceInterface
     *
     * @var TestimonialServiceInterface
     */
    protected $testimonialService;

    /**
     * PictureServiceInterface
     *
     * @var PictureServiceInterface
     */
    protected $pictureService;

    /**
     * TestimonialController constructor.
     *
     * @param TestimonialServiceInterface $testimonialService TestimonialServiceInterface
     * @param PictureServiceInterface     $pictureService     PictureServiceInterface
     */
    public function __construct(
        TestimonialServiceInterface $testimonialService,
        PictureServiceInterface $pictureService
    ) {
        $this->testimonialService = $testimonialService;
        $this->pictureService = $pictureService;
    }

    /**
     * Name store
     *
     * @param SaveTestimonialRequest $request SaveTestimonialRequest
     *
     * @return JsonResponse|object
     */
    public function store(SaveTestimonialRequest $request)
    {

        $fields = $request->validated();

        /**
         * Testimonial
         *
         * @var Testimonial $testimonial
         */
        $testimonial = $this->testimonialService->saveTestimonial(new Testimonial(), $fields);

        $this->pictureService->savePicturesEntity($testimonial, Arr::get($fields, 'pictures', []));

        $testimonial->load('testimonialLangs');

        return (new TestimonialResource($testimonial))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Name show
     *
     * @param mixed $id Testimonial Id
     *
     * @return JsonResponse|object
     */
    public function show($id)
    {
        return (new TestimonialResource(Testimonial::findOrFail($id)))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name update
     *
     * @param SaveTestimonialRequest $request SaveTestimonialRequest
     * @param mixed                  $id      Testimonial Id
     *
     * @return JsonResponse|object
     */
    public function update(SaveTestimonialRequest $request, $id)
    {

        $fields = $request->validated();

        /**
         * Testimonial
         *
         * @var Testimonial
         */
        $testimonial = $this->testimonialService
            ->saveTestimonial(
                Testimonial::findOrFail($id),
                $fields
            );

        $this->pictureService->savePicturesEntity($testimonial, Arr::get($fields, 'pictures', []));

        $testimonial->load('testimonialLangs');

        return (new TestimonialResource($testimonial))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name delete
     *
     * @param mixed $id Testimonial Id
     *
     * @return JsonResponse|object
     * @throws Exception
     */
    public function delete($id)
    {
        /**
         * Testimonial
         *
         * @var Testimonial $testimonial
         */
        $testimonial = Testimonial::findOrFail($id);
        $testimonial->delete();

        return response()->json()->setStatusCode(Response::HTTP_NO_CONTENT);
    }

    /**
     * Name datatableIndex
     *
     * @param DatatableRequest          $request          DatatableRequest
     * @param DatatableServiceInterface $datatableService DatatableServiceInterface
     *
     * @return JsonResponse
     */
    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService)
    {
        $testimonials = Testimonial::query();

        $datatableDTO = $datatableService->getDatatableDTO($request->validated(), $testimonials);

        $collection = $datatableDTO->builder->get();

        $collection
            ->load('testimonialLangs')
            ->load('pictures', 'pictures.pictureLangs');

        $datatableDTO->collection = TestimonialResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }
}
