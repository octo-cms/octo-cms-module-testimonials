<?php

namespace OctoCmsModule\Testimonials\Http\Controllers\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\DTO\EntityIdsDTO;
use OctoCmsModule\Core\Http\Requests\EntityIdsRequest;
use OctoCmsModule\Core\Interfaces\EntityIdsServiceInterface;
use OctoCmsModule\Testimonials\Entities\Testimonial;

/**
 * Class BlockEntityController
 *
 * @package OctoCmsModule\Testimonials\Http\Controllers\V1
 */
class BlockEntityController extends Controller
{
    /**
     * @param EntityIdsRequest          $request
     * @param EntityIdsServiceInterface $entityIdsService
     *
     * @return JsonResponse
     */
    public function blockEntityIds(
        EntityIdsRequest $request,
        EntityIdsServiceInterface $entityIdsService
    ) {
        $fields = $request->validated();

        /**
 * @var EntityIdsDTO $entityIdsDTO
*/
        $entityIdsDTO = $entityIdsService->getEntityIdsDTO(
            Testimonial::query()->whereNotIn('id', Arr::get($fields, 'excludedIds', [])),
            [
                'orderBy'     => 'author',
                'currentPage' => Arr::get($fields, 'currentPage', 1),
                'rowsInPage'  => Arr::get($fields, 'rowsInPage', 12)
            ]
        );

        $entityIdsDTO->collection = $entityIdsService->parseCollection(
            $entityIdsDTO->builder->get(),
            'id',
            'author',
            null
        );

        return response()->json($entityIdsDTO, Response::HTTP_OK);
    }
}
