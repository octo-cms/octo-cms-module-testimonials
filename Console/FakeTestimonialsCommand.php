<?php

namespace OctoCmsModule\Testimonials\Console;

use Exception;
use Illuminate\Console\Command;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use Faker\Generator as Faker;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;

/**
 * Class InstallCoreCommand
 *
 * @package OctoCmsModule\Core\Console
 * @author  danielepasi
 */
class FakeTestimonialsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'fake:testimonials';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * @var SettingServiceInterface $settingService
     */
    protected $settingService;
    protected $faker;

    /**
     * FakeCatalogCommand constructor.
     *
     * @param SettingServiceInterface $settingService
     * @param Faker                   $faker
     */
    public function __construct(SettingServiceInterface $settingService, Faker $faker)
    {
        parent::__construct();
        $this->settingService = $settingService;
        $this->faker = $faker;
    }


    /**
     * @throws Exception
     */
    public function handle()
    {
        $this->info('Running Fake Testimonials Command ...');

        $languages = $this->settingService->getSettingByName(SettingNameConst::LANGUAGES);

        $this->info('Creating Testimonials ...');

        factory(Testimonial::class, 20)
            ->create()
            ->each(
                function (Testimonial $testimonial) use ($languages) {
                    foreach ($languages as $language) {
                        factory(TestimonialLang::class)->create(
                            [
                            'lang'           => $language,
                            'testimonial_id' => $testimonial->id
                            ]
                        );
                    }
                }
            );
    }
}
