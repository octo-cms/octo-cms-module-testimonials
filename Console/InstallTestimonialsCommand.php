<?php

namespace OctoCmsModule\Testimonials\Console;

use Illuminate\Console\Command;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use Faker\Generator as Faker;

/**
 * Class InstallTestimonialsCommand
 *
 * @category Octo
 * @package  OctoCmsModule\Testimonials\Console
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class InstallTestimonialsCommand extends Command
{

    protected const MODULE_NAME = 'Testimonials';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'install:testimonials';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * SettingServiceInterface
     *
     * @var SettingServiceInterface $settingService
     */
    protected $settingService;

    /**
     * Faker
     *
     * @var Faker
     */
    protected $faker;

    /**
     * FakeCatalogCommand constructor.
     *
     * @param SettingServiceInterface $settingService SettingServiceInterface
     * @param Faker                   $faker          Faker
     */
    public function __construct(SettingServiceInterface $settingService, Faker $faker)
    {
        parent::__construct();
        $this->settingService = $settingService;
        $this->faker = $faker;
    }


    /**
     * Name handle
     *
     * @return void
     */
    public function handle()
    {
        $this->info('Running Install Testimonials Command ...');

        $this->info('Creating Settings ...');

        $this->createSettings();
    }

    /**
     * Name createSettings
     *
     * @return void
     */
    private function createSettings()
    {
        $data = [
            'image_width'  => 500,
            'image_height' => 300,
        ];

        Setting::updateOrCreate(
            ['name' => 'module_' . strtolower(self::MODULE_NAME)],
            ['value' => $data]
        );
    }
}
