<?php

namespace OctoCmsModule\Testimonials\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class TestimonialLangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'lang'        => $this->lang,
            'job'         => $this->job,
            'text'        => $this->text,
            'testimonial' => new TestimonialResource($this->whenLoaded('testimonial')),
        ];
    }
}
