<?php

namespace OctoCmsModule\Testimonials\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\PictureResource;

class TestimonialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'author'           => $this->author,
            'testimonialLangs' => TestimonialLangResource::collection($this->whenLoaded('testimonialLangs')),
            'pictures'         => PictureResource::collection($this->whenLoaded('pictures')),
        ];
    }
}
