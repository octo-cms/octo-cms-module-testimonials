<?php

namespace OctoCmsModule\Testimonials\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Testimonials\Entities\Testimonial;

/**
 * Class TestimonialFactory
 *
 * @package OctoCmsModule\Testimonials\Factories
 */
class TestimonialFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Testimonial::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'author' => $this->faker->name,
        ];
    }
}
