<?php

namespace OctoCmsModule\Testimonials\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;

/**
 * Class TestimonialLangFactory
 *
 * @package OctoCmsModule\Testimonials\Factories
 */
class TestimonialLangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TestimonialLang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'testimonial_id' => Testimonial::factory(),
            'lang'           => $this->faker->randomElement(['it', 'en', 'fr']),
            'job'            => $this->faker->jobTitle,
            'text'           => $this->faker->text(50),
        ];
    }
}
