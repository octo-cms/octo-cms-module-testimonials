<?php

namespace OctoCmsModule\Testimonials\Services;

use OctoCmsModule\Core\Services\PictureService as PictureServiceCore;
use OctoCmsModule\Testimonials\Interfaces\PictureServiceInterface;

/**
 * Class PictureService
 *
 * @package OctoCmsModule\Testimonials\Services
 */
class PictureService extends PictureServiceCore implements PictureServiceInterface
{

}
