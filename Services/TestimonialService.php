<?php

namespace OctoCmsModule\Testimonials\Services;

use Illuminate\Support\Arr;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;
use OctoCmsModule\Testimonials\Interfaces\TestimonialServiceInterface;

/**
 *
 * Class TestimonialService
 * @package OctoCmsModule\Testimonials\Services
 */
class TestimonialService implements TestimonialServiceInterface
{
    /**
     * @param Testimonial $testimonial
     * @param array       $fields
     * @return Testimonial
     */
    public function saveTestimonial(Testimonial $testimonial, array $fields): Testimonial
    {
        $testimonial->fill($fields);
        $testimonial->save();

        foreach (Arr::get($fields, 'testimonialLangs', []) as $testimonialLang) {
            TestimonialLang::updateOrCreate(
                [
                    'testimonial_id' => $testimonial->id,
                    'lang'           => Arr::get($testimonialLang, 'lang', ''),
                ],
                [
                    'job'  => Arr::get($testimonialLang, 'job', ''),
                    'text' => Arr::get($testimonialLang, 'text', ''),
                ]
            );
        }

        return $testimonial;
    }
}
