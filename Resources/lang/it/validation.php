<?php

return [
    'author'           => [
        'required' => 'Il campo Author è obbligatorio.',
        'string'   => 'Il campo Author deve essere una stringa.',
    ],
    'testimonialLangs' => [
        'required' => 'Le traduzioni sono obbligatorie.',
        '*'       => [
            'lang' => [
                'required' => 'La lingua è obbligatorie.',
                'string'   => 'La lingua deve essere una stringa.',
            ],
            'job'  => [
                'required' => 'Il campo job è obbligatorio.',
                'string'   => 'Il campo job deve essere una stringa.',
            ],
            'text' => [
                'required' => 'Il campo text è obbligatorio.',
                'string'   => 'Il campo text deve essere una stringa.',
            ]
        ],
    ]
];
