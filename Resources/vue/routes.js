const routes = {
    DATATABLES: {
        testimonials: 'api/admin/v1/datatables/testimonials',
    },

    TESTIMONIAL_STORE: 'api/admin/v1/testimonials',
    TESTIMONIAL_UPDATE: 'api/admin/v1/testimonials/{id}',
    TESTIMONIAL_DELETE: 'api/admin/v1/testimonials/{id}',

    CONFIG_SETTINGS_POST: 'api/admin/v1/settings',
};

export {
    routes
}
