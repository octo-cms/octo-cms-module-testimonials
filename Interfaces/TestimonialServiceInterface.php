<?php

namespace OctoCmsModule\Testimonials\Interfaces;

use OctoCmsModule\Testimonials\Entities\Testimonial;

/**
 * Interface PageServiceInterface
 *
 * @package OctoCmsModule\Admin\Interfaces
 * @author  danielepasi
 */
interface TestimonialServiceInterface
{
    /**
     * @param Testimonial $testimonial
     * @param array       $fields
     *
     * @return Testimonial
     */
    public function saveTestimonial(Testimonial $testimonial, array $fields): Testimonial;
}
