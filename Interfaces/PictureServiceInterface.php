<?php

namespace OctoCmsModule\Testimonials\Interfaces;

/**
 * Interface PictureServiceInterface
 *
 * @package OctoCmsModule\Testimonials\Interfaces
 */
interface PictureServiceInterface extends \OctoCmsModule\Core\Interfaces\PictureServiceInterface
{

}
