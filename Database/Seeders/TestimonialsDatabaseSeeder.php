<?php

namespace OctoCmsModule\Testimonials\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;

/**
 * Class TestimonialsDatabaseSeeder
 *
 * @package OctoCmsModule\Testimonials\Database\Seeders
 */
class TestimonialsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
    }
}
