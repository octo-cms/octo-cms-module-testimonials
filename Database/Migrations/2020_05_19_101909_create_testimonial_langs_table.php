<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestimonialLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonial_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('testimonial_id')->unsigned();
            $table->string('lang', 2);
            $table->string('job', 60);
            $table->text('text');
            $table->timestamps();

            $table->foreign('testimonial_id')->references('id')
                ->on('testimonials')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonial_langs');
    }
}
