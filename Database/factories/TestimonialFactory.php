<?php

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory 
 */

use Faker\Generator as Faker;
use OctoCmsModule\Testimonials\Entities\Testimonial;

$factory->define(
    Testimonial::class, function (Faker $faker) {
        return [
        'author' => $faker->name,
        ];
    }
);
