<?php

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory 
 */

use Faker\Generator as Faker;
use OctoCmsModule\Testimonials\Entities\Testimonial;
use OctoCmsModule\Testimonials\Entities\TestimonialLang;

$factory->define(
    TestimonialLang::class, function (Faker $faker) {
        return [
        'testimonial_id' => factory(Testimonial::class),
        'lang'           => $faker->randomElement(['it', 'en', 'fr']),
        'job'            => $faker->jobTitle,
        'text'           => $faker->text(50),
        ];
    }
);
