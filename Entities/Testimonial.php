<?php

namespace OctoCmsModule\Testimonials\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Traits\PicturableTrait;
use OctoCmsModule\Testimonials\Factories\TestimonialFactory;

/**
 * OctoCmsModule\Testimonials\Entities\Testimonial
 *
 * @property int                                                     $id
 * @property string                                                  $author
 * @property Carbon|null                                             $created_at
 * @property Carbon|null                                             $updated_at
 * @property-read Collection|TestimonialLang[] $testimonialLangs
 * @property-read int|null                     $testimonial_langs_count
 * @method static Builder|Testimonial newModelQuery()
 * @method static Builder|Testimonial newQuery()
 * @method static Builder|Testimonial query()
 * @method static Builder|Testimonial whereAuthor($value)
 * @method static Builder|Testimonial whereCreatedAt($value)
 * @method static Builder|Testimonial whereId($value)
 * @method static Builder|Testimonial whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|Picture[]         $pictures
 * @property-read int|null                     $pictures_count
 */
class Testimonial extends Model
{
    use PicturableTrait, HasFactory;

    public const GCS_PATH = 'testimonials';

    /** @var array  */
    protected $fillable = [
        'author'
    ];

    /**
     * @return TestimonialFactory
     */
    protected static function newFactory()
    {
        return TestimonialFactory::new();
    }

    /**
     * @return HasMany
     */
    public function testimonialLangs()
    {
        return $this->hasMany(TestimonialLang::class);
    }
}
