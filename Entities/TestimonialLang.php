<?php

namespace OctoCmsModule\Testimonials\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use OctoCmsModule\Testimonials\Factories\TestimonialLangFactory;

/**
 * OctoCmsModule\Testimonials\Entities\TestimonialLang
 *
 * @property int                             $id
 * @property int                             $testimonial_id
 * @property string                          $lang
 * @property string                          $job
 * @property string                          $text
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Testimonial                $testimonial
 * @method static Builder|TestimonialLang newModelQuery()
 * @method static Builder|TestimonialLang newQuery()
 * @method static Builder|TestimonialLang query()
 * @method static Builder|TestimonialLang whereCreatedAt($value)
 * @method static Builder|TestimonialLang whereId($value)
 * @method static Builder|TestimonialLang whereJob($value)
 * @method static Builder|TestimonialLang whereLang($value)
 * @method static Builder|TestimonialLang whereTestimonialId($value)
 * @method static Builder|TestimonialLang whereText($value)
 * @method static Builder|TestimonialLang whereUpdatedAt($value)
 * @mixin Eloquent
 */
class TestimonialLang extends Model
{
    use HasFactory;

    /** @var array  */
    protected $fillable = [
        'testimonial_id',
        'text',
        'job',
        'lang'
    ];

    /**
     * @return TestimonialLangFactory
     */
    protected static function newFactory()
    {
        return TestimonialLangFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function testimonial()
    {
        return $this->belongsTo(Testimonial::class);
    }
}
