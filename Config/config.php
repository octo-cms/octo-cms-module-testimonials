<?php

return [
    'name' => 'Testimonials',
    'admin' => [
        'sidebar' => [
            [
                'order'  => 4,
                'label'  => 'testimonials',
                'childs' => [
                    [
                        'label' => 'list',
                        'route' => 'admin.vue-route.testimonials.testimonials',
                    ],
                ],
                'settings' => [
                    [
                        'label' => 'testimonials',
                        'route' => 'admin.vue-route.testimonials.settings',
                    ],
                ],
            ],
        ],
    ],
];
