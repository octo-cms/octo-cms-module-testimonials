<?php

Route::group(
    ['prefix' => env('MIX_ADMIN_PREFIX'), 'middleware' => ['auth-admin']],
    function () {
        Route::group(
            ['prefix' => 'testimonials'],
            function () {
                Route::get(
                    'testimonials',
                    getRouteAction("Testimonials", "VueRouteController", 'testimonials')
                )
                ->name('admin.vue-route.testimonials.testimonials');

                Route::get(
                    'settings',
                    getRouteAction("Testimonials", "VueRouteController", 'settings')
                )
                    ->name('admin.vue-route.testimonials.settings');
            }
        );
    }
);
