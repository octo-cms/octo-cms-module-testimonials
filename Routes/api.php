<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'admin/v1'], function () {

    Route::group(['middleware' => ['auth:sanctum']], function () {

        Route::group(['prefix' => 'testimonials'], function () {

            Route::post('', getRouteAction("Testimonials", "V1\TestimonialController", 'store'))
                ->name('admin.testimonials.store');

            Route::post('block-entity-ids', getRouteAction("Testimonials", "V1\BlockEntityController", 'blockEntityIds'))
                ->name('admin.testimonials.block.entity.ids');

            Route::group(['prefix' => '{id}'], function () {
                Route::get('', getRouteAction("Testimonials", "V1\TestimonialController", 'show'))
                    ->name('admin.testimonials.show');
                Route::delete('', getRouteAction("Testimonials", "V1\TestimonialController", 'delete'))
                    ->name('admin.testimonials.delete');
                Route::put('', getRouteAction("Testimonials", "V1\TestimonialController", 'update'))
                    ->name('admin.testimonials.update');
            });
        });

        Route::group(['prefix' => 'datatables'], function () {
            Route::post('testimonials', getRouteAction("Testimonials", "V1\TestimonialController", 'datatableIndex'))
                ->name('admin.datatables.testimonials');
        });
    });
});
